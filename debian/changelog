tryton-modules-stock-package-shipping-ups (7.0.4-3) unstable; urgency=medium

  * Update the Depends for 7.0.
  * Update the Depends for 7.0.
  * Update the test depends for 7.0.
  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:56:19 +0200

tryton-modules-stock-package-shipping-ups (7.0.4-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:29 +0200

tryton-modules-stock-package-shipping-ups (7.0.4-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083954).
  * Merging upstream version 7.0.4.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 11:53:28 +0200

tryton-modules-stock-package-shipping-ups (6.0.5-1) unstable; urgency=medium

  * Switch to pgpmode=none in the watch file.
  * Merging upstream version 6.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 18 Sep 2024 14:13:38 +0200

tryton-modules-stock-package-shipping-ups (6.0.4-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Merging upstream version 6.0.4.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 18 Apr 2024 10:57:25 +0200

tryton-modules-stock-package-shipping-ups (6.0.3-2) sid; urgency=medium

  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 13 Feb 2023 13:19:00 +0100

tryton-modules-stock-package-shipping-ups (6.0.3-1) unstable; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Merging upstream version 6.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 18 Oct 2022 10:16:46 +0200

tryton-modules-stock-package-shipping-ups (6.0.2-2) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:53:22 +0100

tryton-modules-stock-package-shipping-ups (6.0.2-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.2.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:13:34 +0200

tryton-modules-stock-package-shipping-ups (5.0.7-1) unstable; urgency=medium

  * Merging upstream version 5.0.7.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 04 Oct 2021 15:14:56 +0200

tryton-modules-stock-package-shipping-ups (5.0.5-1) unstable; urgency=medium

  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.5.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 11:38:17 +0200

tryton-modules-stock-package-shipping-ups (5.0.4-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Merging upstream version 5.0.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Dec 2020 09:49:07 +0100

tryton-modules-stock-package-shipping-ups (5.0.3-1) unstable; urgency=medium

  * Merging upstream version 5.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 12:36:58 +0200

tryton-modules-stock-package-shipping-ups (5.0.2-2) unstable; urgency=medium

  * Remove 01-skip-non-idempotent-tests.patch, patch went upstream.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 25 Jul 2019 09:46:25 +0200

tryton-modules-stock-package-shipping-ups (5.0.2-1) unstable; urgency=medium

  * Merging upstream version 5.0.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 23 Jul 2019 19:51:29 +0200

tryton-modules-stock-package-shipping-ups (5.0.1-2) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:17:34 +0200

tryton-modules-stock-package-shipping-ups (5.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 12:10:25 +0100

tryton-modules-stock-package-shipping-ups (5.0.0-3) unstable; urgency=medium

  * Add 01-skip-non-idempotent-tests.patch.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 04 Jan 2019 21:54:36 +0100

tryton-modules-stock-package-shipping-ups (5.0.0-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:35:39 +0100

tryton-modules-stock-package-shipping-ups (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating copyright file.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:08:34 +0100

tryton-modules-stock-package-shipping-ups (4.6.0-2) unstable; urgency=medium

  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:15:16 +0200

tryton-modules-stock-package-shipping-ups (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:20:41 +0100

tryton-modules-stock-package-shipping-ups (4.4.1-3) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:45 +0200

tryton-modules-stock-package-shipping-ups (4.4.1-2) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:03:09 +0200

tryton-modules-stock-package-shipping-ups (4.4.1-1) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).
  * Merging upstream version 4.4.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 02 Jul 2017 17:06:58 +0200

tryton-modules-stock-package-shipping-ups (4.4.0-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.4.0.
  * Updating debian/copyright.
  * Updating Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:30:10 +0200

tryton-modules-stock-package-shipping-ups (4.2.0-1) unstable; urgency=medium

  * Initial commit (Closes: #847185).

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 11 Dec 2016 10:18:40 +0100
